# Leaflet Choropleth interactive map of all US Counties 

- Uses [Leaflet.js](http://leafletjs.com/) library
- GeoJson data obtained using:
  - [US Census Bearue Shape files](https://www.census.gov/cgi-bin/geo/shapefiles2010/main)
  - [Mapshaper (convert shape files into GeoJson)](http://mapshaper.org/)

### [Interactive Demo (click here)](https://cdn.rawgit.com/cryocaustik/leaflet-choropleth/c802950f/choropleth_total_pledge.html)

![Alt text](sample.png?raw=true "Leaflet Choropleth interactive map of all US Counties")
